//gulpfile.js

const gulp = require('gulp');
const { series } = require('gulp');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
var order = require('gulp-order');

const html = () => {
   return gulp.src('./src/index.html')
      .pipe(gulp.dest('dist'));
};

const css = () => {
   return gulp.src('./src/css/**/*.css')
      .pipe(concat('style.css'))
      .pipe(gulp.dest('dist'));
};

const js = () => {
   /* TODO door student, opgave 1b */
   return;
};

const img = () => {
    /* TODO  door student, opgave 1c */
   return;
};

const json = () => {
   return gulp.src('./src/json/images.json')
      .pipe(gulp.dest('dist'));
};

gulp.task('watch', function () {
   browserSync.init({
      server: "./dist/"
   });

   gulp.watch("./src/css/**/*.css", series(css));
   gulp.watch("./src/js/**/*.js", series(js));
   gulp.watch("./src/index.html", series(html));
   gulp.watch("./dist/**/*.*").on('change', browserSync.reload);
});

/* TODO door student, opgave 1a */
exports.build = series(html, css, json);