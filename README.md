# ICT.DT.WEBD.RCS.19.t1.2021

Run instructions

Install git first: https://git-scm.com/downloads

```
$ git clone https://gitlab.com/ekkebus/ict.dt.webd.rcs.19.t1.2021.git
```

Installed NPM packages
Je hebt een installatie van node.js nodig om NPM te kunnen runnen (https://nodejs.org/en/)
```
npm install --save-dev gulp-cli gulp gulp-concat gulp-order browser-sync
```

# Tip van Flip
Zorg bij het maken van de toets dat je altijd papier en een pen bij de hand hebt, je mag gewoon aantekeningen maken op je opgavenblad.

# Opdracht 1 Gulp denkstappen
1. Kijk wat er al aan code gegeven is
1. 1A exports.build uitbreiden
    1. goed kijken wat er al is
    1. 'js' en 'img' const toevoegen als variabelen aan series() functie
1. 1B JS-Task implementeren (n.b. deze moeten een function returnen)
    1. return aanpassen, goed kijken bij css const.
    1. controleer of the 'gulp-order' is geimporteerd
    1. Google https://www.npmjs.com/package/gulp-order
    1. chain .pipe(order()) aan gulp.src(), geef bestandsnamen mee in de volgorde zoals je dat wil. Geef de bestandsnamen mee als een array aan order
    1. chain .pipe(gulp.dest()) als laatste aan deze return waarde
1. 1C IMG-task implementeren
    1. voeg de juiste selector toe die *.jpg uit de src selecteert, kijk af van css
    1. voeg de juiste dest toe.
1. Controleer in de terminal/commandline of gulp build ook daadwerkelijk werkt.
    1. controleer of de volgorde van de JS bestanden is zoals je deze wil hebben
    1. controleer of de bestanden in de juiste folder van dist komen.
    1. als alles loopt, kun je nu de 'gulp watch' runnen.

# Opdracht 2 HTML&CSS denkstappen
1. Kijk wat er al aan code gegeven is
1. 2A Kijk welke divs je logischerwijs kunt veranderen in semantische html5 (e.g. header, footer, etc)
    1. De CSS class namen geven je al wat hints, nu nog de juiste HTML5 tags
    1. Laat de css classes staan, deze heeft de CSS nodig (mag je ook aanpassen)
1. 2B responsiveness
    1. Bekijk hoe de bestaande HTML en CSS in elkaar zit, er wordt gebruik gemaakt van een flexbox. Het bovenste gedeelte van de CSS defineert dus hoe het document eruit ziet als het scherm 'smaller' is dan 800 pixels, wat je moet toevoegen is de mobile view.
    1. Je kunt gebruik maken van de flex direction om de desktop view voor elkaar te krijgen. Standaard is dit een row ('horizonaal').
    1. Voeg onderaan style.css een een mediaquery toe (https://www.w3schools.com/cssref/css3_pr_mediaquery.asp).
    1. Als je de CSS mobile first wil maken, dan defineer je de desktop view in de media query (daarzet je dan row neer voor de flex-direction), in de .container zet je dan flex-direction column neer.
1. 2C transformatie
    1. Kijk goed wat er momenteel met de afbeelding gebeurt, hij is helemaal ingezoomd (scale(5)) (https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/scale)
    1. Je kunt deze opdracht uitvoeren met een :hover, waarbij je de scale '1' maakt.

# Opdracht 3 OO en prototype Denkstappen
1. Kijk wat er al aan code gegeven is
1. Bekijk maak gebruik van het klasse diagram in opgave 4 voor het overzicht.
1. 3A Class contructor
    1. Voeg een klasse toe in ZoomImage.js (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes)
1. 3B Error parameter bij parameter null
    1. Voeg in de constructor een if-statement toe met een OR die controleert op null, die een Error throwed als de waarde true is (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/throw)
1. 3C implementeer getCredits()
    1. Voeg een methode toe aan de klasse, die één String terug geeft waarbij gebruik wordt maakt van de naam en author properties van de klasse die zijn geset in de constructor. Je kunt hier gebruik maken van de Template String (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)
1. 3D schrijf een test voor getCredits()
    1. Kijk wat er al beschikbaar is in ./test/test.html
    1. Controleer of de te testen klasse ook beschikbaar is in dit bestand
    1. In de bestaande describe voeg een 'it' statement toe
        1. Voeg hierbinnen zelf een expected naam, author en creditString
        1. Initieer een ZoomImage klasse waarin je de expected naam en author mee geeft en roep vervolgens in de instantie van deze klasse de getCredits() methode aan. Controleer met een 'expect to be' statement (https://jasmine.github.io/tutorials/your_first_suite)
    1. Open ./test/test.html in je browser en controleer of je test juist runt (of alles groen is)

# Opdracht 4 Module pattern, Promise en AJAX Denkstappen
1. Kijk wat er al aan code gegeven is
1. 4A implementeer de SPA.Game als module pattern, niet alles klasse (https://ultimatecourses.com/blog/mastering-the-module-pattern)
    1. Overweeg of je jQuery wil meegeven aan de zelf-uitvoerende functie van deze module, hiermee kun je voordeel hebben tijdens de implementatie (https://sarfraznawaz.wordpress.com/2012/01/26/javascript-self-invoking-functions/)
    1. Voeg een return met een Oject literal toe. Dit zijn de methodes die worden gereturned door deze module (de methodes zelf moeten we nog implementeren)
    1. Voeg een variabele (met dezelfde naam als hierboven) die een function bevat met de de gevraagde parameters.
1. 4B implementeer SPA module met het module pattern
    1. Please note: je gaat een nieuwe module toevoegen, dit is dus geen ondedeel van SPA.Game. Maar maakt er wel gebruik van.
    1. Voeg een return met een Oject literal toe en implementeer de init functie zoals gevraagd.
    1. Roep de SPA.Game.init() aan met de gevraagde parameters
1. 4C initieer de SPA module
    1. Controleer of de JS modules beschikbaar zijn in het index.html bestand
    1. Kijk goed welke waarde je gaat meegeven aan de init methode, kijk hier in het HTML bestand. In dit geval is het een 'class' en geen ID, niet logisch maar het is wel zo :)   
    1. SPA.init() aan en geef de container selector mee als String ('.container').
1. 4D Implementeer de SPA.Game verder
    1. Voeg een variabele toe aan SPA.game
    1. Als je jQuery heb toegevoegd aan je module, kun je heel mooi gebruik maken van $.getJSON(). Als je deze methode namelijk in zijn geheel returned in deze functie hebt je automatisch een promise. (https://api.jquery.com/jquery.getjson/)
    Als je geen jQuery hebt heeft JS zelf een mooie methode fetch(), hier moet je nog wel zelf de json parsen (https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch), als je deze methode returned heb je ook automatisch een Promise. 
    Binnen de bovenstaande gereturned methode zelf, itereer je door de json heen en return je de array met ZoomImage objecten.
    1. Roep loadImage() methode aan. Omdat deze methode zelf een promise is, kun je de array met ZoomImage objecten verkrijgen via de .then(). Binnen de functie in de then() wijs je de waarde van array toe aan zoomImages en itereer je vervolgens door de array heen om de waarde vervolgens toe tevoegen aan de container. Maak hier gebruik van de aanwezige voorbeeld code in index.html
    En vergeet natuurlijk niet voordat je nieuwe elementen gaat toevoegen aan de container, deze eerst empty te maken. Hier kan jQuery je ook weer erg handig helpen.
